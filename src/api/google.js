const express = require('express')
const router = express.Router()

const app = express()

router.get('/', (req, res) => res.send('Well, hello there!'))
router.get('/search/', (req, res) => {
  res.send(`You need to tell me what you are searching for!`)
})
router.get('/search/:searchParam', (req, res) => {
  res.send(`<a href>https://lmgtfy.app/?q=${req.params.searchParam}</a>`)
})

module.exports = router