const express = require('express')
const googleRoutes = require('./api/google')
const http = require('http');
const port = 8080

const app = express()
app.use(googleRoutes)

const httpServer = http.createServer(app);
httpServer.listen(port);
console.log(`[test api] http server listening at port ${port}`);


module.exports = {
  app
}